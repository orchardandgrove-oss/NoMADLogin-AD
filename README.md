# NoMAD Login AD

This is no longer the repo for NoMAD Login AD.

The NoMAD Login AD repo has moved to [github.com](https://github.com/jamf/NoMADLogin-AD). All support and development is now being done by Jamf. This archive will eventually go away, handle your affairs accordingly.
